package com.example.testcloudstream;

import java.util.function.Function;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class MessageProcessor {

    @Bean
    public Function<KStream<String, Message>, KStream<String, Object>[]> process() {
        return input -> input.map((k,v) -> {
            if(v.msg.equalsIgnoreCase("error")) {
                return new KeyValue<>(k, new Error(v.msg));
            }else{
                v.msg = v.msg.toUpperCase();
                return new KeyValue<>(k, v);
            }
        }).branch((k,v) -> v instanceof Message, (k, v) -> v instanceof Error);
    }

}
