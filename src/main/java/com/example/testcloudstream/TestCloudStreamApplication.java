package com.example.testcloudstream;

import java.io.IOException;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.serializer.JsonSerde;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
public class TestCloudStreamApplication {

    // https://github.com/spring-cloud/spring-cloud-stream-binder-kafka/issues/810

    public static void main(String[] args) {
        SpringApplication.run(TestCloudStreamApplication.class, args);
    }


    @Bean
    public Serde<Message> messageSerde() {
        return new Serde<>() {
            private ObjectMapper objectMapper = new ObjectMapper();

            @Override
            public Serializer<Message> serializer() {
                return (t, d) -> {
                    if (d == null) {
                        return null;
                    }
                    try {
                        return objectMapper.writeValueAsBytes(d);
                    } catch (JsonProcessingException e) {
                        throw new SerializationException("Error serializing JSON message", e);
                    }
                };
            }

            @Override
            public Deserializer<Message> deserializer() {
                return (topic, data) -> {
                    if (data == null) {
                        return null;
                    }
                    try {
                        return objectMapper.readValue(data, Message.class);
                    } catch (IOException e) {
                        throw new SerializationException(e);
                    }
                };
            }
        };
    }

    @Bean
    public Serde<Error> errorSerde() {
        return new Serde<>() {
            private ObjectMapper objectMapper = new ObjectMapper();

            @Override
            public Serializer<Error> serializer() {
                return (t, d) -> {
                    if (d == null) {
                        return null;
                    }
                    try {
                        return objectMapper.writeValueAsBytes(d);
                    } catch (JsonProcessingException e) {
                        throw new SerializationException("Error serializing JSON message", e);
                    }
                };
            }

            @Override
            public Deserializer<Error> deserializer() {
                return (topic, data) -> {
                    if (data == null) {
                        return null;
                    }
                    try {
                        return objectMapper.readValue(data, Error.class);
                    } catch (IOException e) {
                        throw new SerializationException(e);
                    }
                };
            }
        };
    }

}
